{
   config,
   pkgs,
   lib,
   ...
}: {
   home = {
      enableNixpkgsReleaseCheck = true;
      packages = with pkgs; [
         # utils
         alacritty
         curl
         htop
         kakoune
         mosh
         openssh
         ripgrep

         # audio
         ffmpeg

         # video
         imagemagick

         # dev
         git
         nodePackages.vscode-css-languageserver-bin
         nodePackages.vscode-html-languageserver-bin
         nodePackages.vscode-json-languageserver-bin
         typescript
         bun
      ];
      activation = lib.mkIf pkgs.stdenv.isDarwin {
         copyApplications = let
            apps = pkgs.buildEnv {
               name = "home-manager-applications";
               paths = config.home.packages;
               pathsToLink = "/Applications";
            };
         in
            lib.hm.dag.entryAfter ["writeBoundary"] ''
               baseDir="/nix/Applications/alexcf/Home Manager Apps"
               if [ -d "$baseDir" ]; then
                  rm -rf "$baseDir"
               fi
               mkdir -p "$baseDir"
               for appFile in ${apps}/Applications/*; do
                  target="$baseDir/$(basename "$appFile")"
                  $DRY_RUN_CMD cp ''${VERBOSE_ARG:+-v} -RLf "$appFile" "$baseDir"
                  $DRY_RUN_CMD chmod ''${VERBOSE_ARG:+-v} -R +w "$target"
               done
            '';
      };
      file = {
      };
      sessionPath = ["$HOME/bin" "/opt/homebrew/bin"];
      sessionVariables = {
         LC_ALL = "en_US.UTF-8";
         LESSHISTFILE = "/dev/null";
         TZ = "America/Phoenix";
         VISUAL = "kak";
       };
       shellAliases = {
         ls = "ls -aFGhl";
         motd = "$VISUAL $HOME/Documents/motd.txt";
         todo = "$VISUAL $HOME/TODO.md";
         vi = "$VISUAL";
       };
    stateVersion = "23.05";
  };
  programs = {
      alacritty = {
         enable = true;
         settings = {
            colors = {
               primary = {
                  background = "#1c2023";
                  foreground = "#c7ccd1";
               };
               normal = {
                  black = "#1c2023";
                  red = "#c7ae95";
                  green = "#95c7ae";
                  yellow = "#aec795";
                  blue = "#ae95c7";
                  magenta = "#c795ae";
                  cyan = "#95aec7";
                  white = "#c7ccd1";
               };
               bright = {
                  black = "#747c84";
                  red = "#c7ae95";
                  green = "#95c7ae";
                  yellow = "#aec795";
                  blue = "#ae95c7";
                  magenta = "#c795ae";
                  cyan = "#95aec7";
                  white = "#f3f4f5";
               };
            };
            window = {
               dimensions = {
                  columns = 96;
                  lines = 16;
               };
               padding = {
                  x = 10;
                  y = 10;
               };
            };
            font = {
               normal = {
                  family = "Menlo";
               };
               size = 15;
            };
            cursor = {
               style = {
                  blinking = "On";
               };
            };
         };
      };
      zsh = {
         enable = true;
         initExtra = ''
           set -o emacs

           fmt -80 ~/Documents/motd.txt

           update() {
                   sudo -i nix-channel --update
                   sudo -i nix-env -iA nixpkgs.nix
                   sudo -i launchctl remove org.nixos.nix-daemon
                   sudo -i launchctl load /Library/LaunchDaemons/org.nixos.nix-daemon.plist
                   nix flake update "$HOME/.config/darwin/"
                   darwin-rebuild switch --flake "$HOME/.config/darwin/"
                   /opt/homebrew/bin/brew upgrade
           }

            PS1='%1d %B%F{cyan}λ%f%F{magenta}=%f%b '
         '';
       };
   };
}

