{
   config,
   pkgs,
   ...
}: {
   networking = {
      computerName = "MacBook-Pro";
   };

   environment.darwinConfig = "\$HOME/.config/darwin/configuration.nix";

   nix = {
      package = pkgs.nixVersions.stable;
      extraOptions = ''
         experimental-features = nix-command flakes
      '';
      gc = {
         automatic = true;
      };
      settings = {
         auto-optimise-store = true;
         trusted-users = ["alexcf"];
      };
   };

   nixpkgs.config = {
      allowUnfree = true;
      allowUnsupportedSystem = true;
   };

   environment.systemPackages = with pkgs; [
   ];

   environment.shells = [pkgs.bashInteractive pkgs.zsh];

   fonts = {
      fontDir.enable = true;
      fonts = [
         pkgs.ibm-plex
         pkgs.mononoki
         pkgs.roboto
         pkgs.fira
      ];
   };

   services = {
      nix-daemon.enable = true;
   };

   system = {
      activationScripts.applications.text = pkgs.lib.mkForce ''
         echo "setting up /nix/Applications/Nix Apps..." >&2
         rm -rf /nix/Applications/Nix\ Apps
         mkdir -p /nix/Applications/Nix\ Apps
         for app in $(find ${config.system.build.applications}/Applications -maxdepth 1 -type l); do
            src="$(/usr/bin/stat -f%Y "$app")"
            cp -RLf "$src" /nix/Applications/Nix\ Apps
         done
      '';
      defaults = {
         NSGlobalDomain = {
            AppleInterfaceStyle = "Dark";
            AppleKeyboardUIMode = 3;
            AppleMetricUnits = 1;
            InitialKeyRepeat = 15;
            KeyRepeat = 2;
            NSAutomaticCapitalizationEnabled = false;
            NSAutomaticDashSubstitutionEnabled = false;
            NSAutomaticPeriodSubstitutionEnabled = false;
            NSAutomaticQuoteSubstitutionEnabled = false;
            NSAutomaticSpellingCorrectionEnabled = false;
         };
         dock = {
            autohide = true;
            autohide-delay = 0.0;
            autohide-time-modifier = 0.1;
            mru-spaces = false;
            orientation = "left";
            show-recents = false;
         };
         finder = {
            CreateDesktop = false;
            FXDefaultSearchScope = "SCcf";
            FXPreferredViewStyle = "Nlsv";
            ShowPathbar = true;
         };
         screencapture = {
            disable-shadow = true;
            location = "~/Pictures/Screenshots";
         };
      };
      stateVersion = 4;
   };

   users.users = {
      alexcf = {
         home = "/Users/alexcf";
         shell = pkgs.zsh;
      };
   };

   homebrew = {
      enable = true;
      onActivation = {
         autoUpdate = true;
         cleanup = "zap";
      };
      taps = [
         "homebrew/cask"
         "homebrew/cask-drivers"
         "homebrew/services"
      ];
      brews = [
      ];
      casks = [
         # "1password"
         # "ableton-live-10-suite"
         # "alacritty"
         # "focusrite-control"
         # "google-chrome"
         # "obs"
         # "sequel-pro"
         # "slack"
         # "visual-studio-code"
      ];
      extraConfig = ''
         cask_args appdir: "~/Applications/Homebrew Casks"#, require_sha: true
      '';
   };
   programs.zsh.enable = true;
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
}
